import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { InstructionsComponent } from './instructions/instructions.component';

const routes: Routes = [
  { path: '', redirectTo: 'instructions', pathMatch: 'full' },
  { path: 'instructions', component: InstructionsComponent },
  { path: 'posts', loadChildren: () => import(`./articles/articles.module`).then(m => m.ArticlesModule)},
  { path: 'reactive-form', loadChildren: () => import(`./employee-reactive-form/employee-reactive-form.module`)
      .then(m => m.EmployeeReactiveFormModule)},
  { path: 'template-driven-form', loadChildren: () => import(`./employee-template-driven-form/employee-template-driven-form.module`)
      .then(m => m.EmployeeTemplateDrivenFormModule)}
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
