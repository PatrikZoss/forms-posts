import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ListOfArticlesComponent} from './list-of-articles/list-of-articles.component';
import {CommonModule} from '@angular/common';
import {DetailOfArticleComponent} from './detail-of-article/detail-of-article.component';

const routes: Routes = [
  {path: '', component: ListOfArticlesComponent},
  {path: ':id', component: DetailOfArticleComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes), CommonModule],
  exports: [RouterModule],
  declarations: [ListOfArticlesComponent, DetailOfArticleComponent]
})
export class ArticlesModule {
}
