import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailOfArticleComponent } from './detail-of-article.component';

describe('DetailOfArticleComponent', () => {
  let component: DetailOfArticleComponent;
  let fixture: ComponentFixture<DetailOfArticleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailOfArticleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailOfArticleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
