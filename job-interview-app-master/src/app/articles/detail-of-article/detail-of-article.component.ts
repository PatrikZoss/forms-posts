import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {ArticlesRestService} from '../../shared/services/articles-rest.service';
import {Observable} from 'rxjs';
import {ArticleData} from '../../shared/types/article-data';
import {CommentData} from '../../shared/types/comment-data';

@Component({
  selector: 'app-detail-of-article',
  templateUrl: './detail-of-article.component.html',
  styleUrls: ['./detail-of-article.component.scss']
})
export class DetailOfArticleComponent implements OnInit {

  article$: Observable<ArticleData>;

  comments$: Observable<CommentData[]>;

  articleId: number;

  constructor(private route: ActivatedRoute,
              private articlesRestService: ArticlesRestService) {
  }

  ngOnInit() {
    this.articleId = this.route.snapshot.params['id'];
    this.article$ = this.articlesRestService.getArticle(this.articleId);
    this.comments$ = this.articlesRestService.getComments(this.articleId);
  }

}
