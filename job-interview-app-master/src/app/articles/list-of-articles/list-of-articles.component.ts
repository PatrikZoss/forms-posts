import {Component, OnInit} from '@angular/core';
import {ArticlesRestService} from '../../shared/services/articles-rest.service';
import {Observable} from 'rxjs';
import {ArticleData} from '../../shared/types/article-data';

@Component({
  selector: 'app-list-of-articles',
  templateUrl: './list-of-articles.component.html',
  styleUrls: ['./list-of-articles.component.scss']
})
export class ListOfArticlesComponent implements OnInit {

  articles$: Observable<ArticleData[]>;

  constructor(private articlesRestService: ArticlesRestService) {
  }

  ngOnInit() {
    this.articles$ = this.articlesRestService.getArticles();
  }

}
