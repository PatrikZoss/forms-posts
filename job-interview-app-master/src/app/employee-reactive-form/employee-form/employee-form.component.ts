import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {FormHelperService} from '../../shared/services/form-helper.service';
import {EmployeeData} from '../../shared/types/employee-data';

@Component({
  selector: 'app-employee-form',
  templateUrl: './employee-form.component.html',
  styleUrls: ['./employee-form.component.scss']
})
export class EmployeeFormComponent implements OnInit {

  employeeForm: FormGroup;

  employeesList: Array<EmployeeData> = [];

  constructor(private formBuilder: FormBuilder,
              private formHelperService: FormHelperService) {
  }

  ngOnInit() {
    this.employeeForm = this.formBuilder.group({
      name: ['', Validators.required],
      surname: [],
      birthDate: [],
      street: [],
      suite: [],
      city: [],
      zipCode: [],
      phone: [],
      website: [],
      email: [],
      position: []
    });
  }

  saveEmployee() {
    this.formHelperService.validateAllFormFields(this.employeeForm);
    if (this.employeeForm.invalid) {
      return;
    }
    this.employeesList.push(this.employeeForm.value);
  }

}
