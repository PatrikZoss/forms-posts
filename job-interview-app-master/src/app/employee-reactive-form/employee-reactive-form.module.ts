import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {EmployeeFormComponent} from './employee-form/employee-form.component';
import { BasicInformationComponent } from './employee-form/basic-information/basic-information.component';
import { AddressComponent } from './employee-form/address/address.component';
import { ContactInformationComponent } from './employee-form/contact-information/contact-information.component';
import {ReactiveFormsModule} from '@angular/forms';
import {SharedModule} from '../shared/shared.module';

const routes: Routes = [
  {path: '', component: EmployeeFormComponent}
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
    CommonModule,
    ReactiveFormsModule,
    SharedModule],
  exports: [RouterModule],
  declarations: [
    EmployeeFormComponent,
    BasicInformationComponent,
    AddressComponent,
    ContactInformationComponent]
})
export class EmployeeReactiveFormModule {
}
