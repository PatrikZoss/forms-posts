import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {EmployeeTemplateFormComponent} from './employee-template-form/employee-template-form.component';
import {RouterModule, Routes} from '@angular/router';
import {FormsModule} from '@angular/forms';
import {SharedModule} from '../shared/shared.module';

const routes: Routes = [
  {path: '', component: EmployeeTemplateFormComponent}
];

@NgModule({
  declarations: [EmployeeTemplateFormComponent],
    imports: [
        RouterModule.forChild(routes),
        CommonModule,
        FormsModule,
        SharedModule
    ],
  exports: [RouterModule],
})
export class EmployeeTemplateDrivenFormModule {
}
