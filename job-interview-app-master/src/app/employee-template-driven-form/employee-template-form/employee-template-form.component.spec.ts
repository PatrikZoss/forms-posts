import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeeTemplateFormComponent } from './employee-template-form.component';

describe('EmployeeTemplateFormComponent', () => {
  let component: EmployeeTemplateFormComponent;
  let fixture: ComponentFixture<EmployeeTemplateFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmployeeTemplateFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeeTemplateFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
