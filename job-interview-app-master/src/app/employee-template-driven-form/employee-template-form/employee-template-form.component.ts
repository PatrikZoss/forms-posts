import {Component, OnInit} from '@angular/core';
import {EmployeeData} from '../../shared/types/employee-data';

@Component({
  selector: 'app-employee-template-form',
  templateUrl: './employee-template-form.component.html',
  styleUrls: ['./employee-template-form.component.scss']
})
export class EmployeeTemplateFormComponent implements OnInit {

  employee: {} = {};

  employeesList: Array<EmployeeData> = [];

  constructor() {
  }

  ngOnInit() {
  }

  onSubmitTemplateBased() {
    this.employeesList.push(Object.assign({}, this.employee) as EmployeeData);
  }

}
