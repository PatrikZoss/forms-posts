import {Component, Input, OnInit} from '@angular/core';
import {EmployeeData} from '../../types/employee-data';

@Component({
  selector: 'app-list-of-employees',
  templateUrl: './list-of-employees.component.html',
  styleUrls: ['./list-of-employees.component.scss']
})
export class ListOfEmployeesComponent implements OnInit {

  @Input()
  employeesList: Array<EmployeeData> = [];

  constructor() {
  }

  ngOnInit() {
  }

  removeEmployee(employee: EmployeeData) {
    this.employeesList.splice(this.employeesList.indexOf(employee), 1);
  }

}
