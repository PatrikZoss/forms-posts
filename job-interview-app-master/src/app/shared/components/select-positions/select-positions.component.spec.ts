import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectPositionsComponent } from './select-positions.component';

describe('SelectPositionsComponent', () => {
  let component: SelectPositionsComponent;
  let fixture: ComponentFixture<SelectPositionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectPositionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectPositionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
