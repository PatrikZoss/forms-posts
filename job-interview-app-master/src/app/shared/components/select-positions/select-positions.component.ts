import {Component, Input, OnInit} from '@angular/core';
import {FormGroup} from '@angular/forms';

@Component({
  selector: 'app-select-positions',
  templateUrl: './select-positions.component.html',
  styleUrls: ['./select-positions.component.scss']
})
export class SelectPositionsComponent implements OnInit {

  @Input()
  employeeForm: FormGroup;

  constructor() { }

  ngOnInit() {
  }

}
