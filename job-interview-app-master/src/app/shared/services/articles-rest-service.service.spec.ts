import { TestBed } from '@angular/core/testing';

import { ArticlesRestService } from './articles-rest.service';

describe('ArticlesRestServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ArticlesRestService = TestBed.get(ArticlesRestService);
    expect(service).toBeTruthy();
  });
});
