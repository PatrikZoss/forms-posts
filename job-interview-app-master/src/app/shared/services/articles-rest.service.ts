import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import {ArticleData} from '../types/article-data';
import {CommentData} from '../types/comment-data';

@Injectable({
  providedIn: 'root'
})
export class ArticlesRestService {

  private static API_ENDPOINT = 'https://jsonplaceholder.typicode.com/';

  constructor(private http: HttpClient) {
  }

  getArticles(): Observable<ArticleData[]> {
    return this.http.get<ArticleData[]>(ArticlesRestService.API_ENDPOINT + 'posts');
  }

  getArticle(id: number): Observable<ArticleData> {
    return this.http.get<ArticleData>(ArticlesRestService.API_ENDPOINT + 'posts/' + id);
  }

  getComments(id: number): Observable<CommentData[]> {
    const params = new HttpParams()
      .set('postId', String(id));
    return this.http.get<CommentData[]>(ArticlesRestService.API_ENDPOINT + 'comments', {params});
  }

}
