import { TestBed } from '@angular/core/testing';

import { PositionsRestService } from './positions-rest.service';

describe('PositionsRestService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PositionsRestService = TestBed.get(PositionsRestService);
    expect(service).toBeTruthy();
  });
});
