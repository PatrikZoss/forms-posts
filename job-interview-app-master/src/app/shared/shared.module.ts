import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SelectPositionsComponent} from './components/select-positions/select-positions.component';
import {ReactiveFormsModule} from '@angular/forms';
import {ArticlesRestService} from './services/articles-rest.service';
import {FormHelperService} from './services/form-helper.service';
import {NavbarComponent} from './navbar/navbar.component';
import {RouterModule} from '@angular/router';
import { ListOfEmployeesComponent } from './components/list-of-employees/list-of-employees.component';


@NgModule({
  declarations: [
    SelectPositionsComponent,
    NavbarComponent,
    ListOfEmployeesComponent],
    exports: [
        SelectPositionsComponent,
        NavbarComponent,
        ListOfEmployeesComponent
    ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    RouterModule
  ],
  providers: [
    ArticlesRestService,
    FormHelperService,
  ]
})
export class SharedModule {
}
