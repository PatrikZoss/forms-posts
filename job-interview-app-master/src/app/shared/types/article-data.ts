import {AbstractData} from './abstract-data';

export interface ArticleData extends AbstractData {

  userId: number;

  body: string;

  title: string;

}
