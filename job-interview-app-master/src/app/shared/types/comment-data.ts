import {AbstractData} from './abstract-data';

export interface CommentData extends AbstractData {

  postId: number;

  name: string;

  email: string;

  body: string;

}
