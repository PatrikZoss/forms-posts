export interface EmployeeData {

  name: string;

  surname: string;

  birthDate: Date;

  position: string;

  street: string;

  suite: string;

  city: string;

  zipCode: number;

  phone: string;

  email: string;

  website: string;

}
